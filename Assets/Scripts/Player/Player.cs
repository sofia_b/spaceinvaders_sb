﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : PhysicsObject
{
     public Bullet Bullet;
     public Color PlayerColor;
    // private float _speed = 0.6f;
     private Vector2 _bulletPosition = new Vector2(0,0);

    //public float maxSpeed = 7;
    //public float jumpTakeOffSpeed = 7;

    private void Update()
    {
        moveToSides();
    }
   
    private void moveToSides() {
        if (Input.GetKey(KeyCode.A))
        {
            this.gameObject.transform.position -= new Vector3(0.05f, 0, 0);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.gameObject.transform.position -= new Vector3(0.05f, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.gameObject.transform.position += new Vector3(0.05f, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.gameObject.transform.position += new Vector3(0.05f, 0, 0);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            shoot();
        }
    }

    

    private void shoot() {
        Bullet e = Bullet.Instantiate(Bullet);
        e.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y+0.15f, 0);
        e.isEnemyBullet = false;
        e.GetComponent<SpriteRenderer>().material.color = PlayerColor;

    }


}
