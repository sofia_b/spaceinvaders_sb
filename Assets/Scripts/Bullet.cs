﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float initialPosition;
    public float speed = 0.01f;
    private float MaxDistanceEnemyBullet = -5;
    private float MaxDistancePlayerBullet = 5;
    public bool isEnemyBullet = false;

    private void FixedUpdate()
    {
        moveInShoot(isEnemyBullet);
        calculateDistanceToDestroy();
    }


    private void moveInShoot(bool isEnemyBullet) {
        if(isEnemyBullet)
            transform.Translate(0, -speed, 0);
        else
            transform.Translate(0, speed, 0);

    }

    //this void is to calculate the max dis
    private void calculateDistanceToDestroy() {

        if (isEnemyBullet && transform.position.y <= MaxDistanceEnemyBullet)
            Destroy(this.gameObject);

        if (!isEnemyBullet && transform.position.y >= MaxDistancePlayerBullet)
            Destroy(this.gameObject);
    }
    
   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("collision: " + collision.gameObject.name);
        Destroy(this.gameObject);
    }
}
