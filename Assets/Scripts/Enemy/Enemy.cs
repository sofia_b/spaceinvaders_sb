﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Enemy : Singleton<Enemy>
{
    public Sprite EnemySprite;
    public Bullet Bullet;
    public int _EnemyLife = 0;
    public Color _EnemyColor;
    public bool needsToShoot = false;
    public bool isAlive = true;
    private float bulletInitialPosition;
    private int _timeToShoot = 3;
    private float _time = 0;
    public Vector2 enemyGridPos;

    public delegate void OnEnemyDie();
    public static event OnEnemyDie OnEnemyDie_;
    

    //initial settings of an enemy with random color selection
    public void SetEnemyProperties(int [] _life, Color [] _color, Vector2 gridPos) {

        int SelectedValue = setRandomEnemy();
        _EnemyLife = _life[SelectedValue];
        _EnemyColor = _color[SelectedValue];        
        this.GetComponent<SpriteRenderer>().material.color = _EnemyColor;
        enemyGridPos = gridPos;
       
       
    }

    

    private void Start()
    {
        // shoot();//only for initial test ^ ^        
        OnEnemyDie_ = die;
    }
    

    private void Update()
    {
        if (_EnemyLife > 0)
        {
            if(needsToShoot)
            countTimeToShoot();            
        }else{
            die();
        }
    }

    public void setEnableParameters(bool val) {
        this.gameObject.GetComponent<SpriteRenderer>().enabled = val;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = val;
    }
    //int function to give a value abd choose one of the enemies parameters to set.
    private int setRandomEnemy()
    {
        int value = UnityEngine.Random.Range(0, 4);
        return value;
    }

    private void countTimeToShoot() {
        _time += Time.deltaTime;
        if (_time >= _timeToShoot)
        {
            shoot();
            _time -= _time;
            _timeToShoot = settimeToShoot();
        }
    }
    //void to instantiate a bullet for shoot.
    private void shoot()
    {
        Bullet e = Bullet.Instantiate(Bullet);
        e.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y+0.15f, 0);
        e.isEnemyBullet = true;
        e.gameObject.tag = "EnemyBullet";
        e.GetComponent<SpriteRenderer>().material.color = _EnemyColor;
    }

    //int function to set time to shoot randomly between 
    public int settimeToShoot()
    {
        int time = UnityEngine.Random.Range(0,3);
        return time;
    }

    public void OnEnemyActive() {


    }

    
    
    
    private void die()
    {
        setEnableParameters(false);
        isAlive = false;

        EnemiesController.Instance.onAdjacentEnemyDies_(enemyGridPos);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name != "floor" && collision.gameObject.tag == "PlayerBullet")
        {
            Debug.Log("PLAYER - collision: " + collision.gameObject.name);
            if(needsToShoot)
                _EnemyLife--;
        }
    }

}
