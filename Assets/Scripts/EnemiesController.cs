﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesController : Singleton<EnemiesController>
{


    public Enemy EnemiesPrefab;
    public Color[] EnemyColors;
    public int[] EnemyLife;

    public int Rows = 4;
    public int Columns = 10;
    public int TotalEnemiesToShow;
    public GameObject GameOverPanel;
    public Enemy[,] enemyArray_ = new Enemy[4, 10];
    private List<Enemy> enemyList = new List<Enemy>();
    public Dictionary<int, Enemy> enemyColumnsList = new Dictionary<int, Enemy>();// column, enemies
    private GameObject _EnemiesParent;
    private Vector2 _initialposition = new Vector2(-4, 5f);//-5,47 , 3,61
    private float _incrementalPosParameter = 0;
    private bool isGamePlayActive = true;

    protected EnemiesController() { }
    public GameObject[] Walls;

    float newPosX = -0.05f;
    float newPosY = 0;
    bool direction = true;

    private void Start()
    {
       
    }
    public void initialSetup()
    {
        setInitialGrid();
    }


    //Set initial grid with enemies in order of rows and columns
    public void setInitialGrid() {
        lastRowToShoot = Rows - 1;
        for (int iRows = 0; iRows < Rows; iRows++) {
            _incrementalPosParameter = 0;
            _incrementalPosParameter -= 1f;
            _initialposition.y += _incrementalPosParameter;

            for (int jColums = 0; jColums < Columns; jColums++)
            {
                Enemy e_ = createEnemyInstance(_initialposition.x + _incrementalPosParameter, _initialposition.y, new Vector2(iRows, jColums));
                e_.setEnableParameters(true);
                enemyList.Add(e_);
                _incrementalPosParameter += 1f;
                enemyArray_[iRows, jColums] = e_;
            }
        }
    }

    

    public void onAdjacentEnemyDies_(Vector2 gridPos) {
       
        int EnemyX = (int)gridPos.x;
        int EnemyY = (int)gridPos.y;
        Debug.Log("gridPos: " + gridPos+ "Rows: " + Rows);
        Color colorToDestroy = enemyArray_[EnemyX, EnemyY]._EnemyColor;


        if (gridPos.x > 0) { 
            Enemy enemyTop = enemyArray_[EnemyX - 1, EnemyY];
            if (gridPos.x > 0 && enemyTop._EnemyColor == colorToDestroy && enemyTop.isAlive == true)
                enemyTop.setEnableParameters(false);
        }

        if (gridPos.x < Rows - 1) { 
           Enemy enemyBottom = enemyArray_[EnemyX + 1, EnemyY];
           if(enemyBottom._EnemyColor == colorToDestroy && enemyBottom.isAlive == true)
                enemyBottom.setEnableParameters(false);
        }

        if (gridPos.y < Columns - 1) {       
            Enemy enemyRight = enemyArray_[EnemyX, EnemyY + 1];
            if ( enemyRight._EnemyColor == colorToDestroy && enemyRight.isAlive == true)
                enemyRight.setEnableParameters(false);
        }
        if (gridPos.y > 0) { 
           Enemy enemyLeft = enemyArray_[EnemyX, EnemyY - 1];
           if (gridPos.y > 0 && enemyLeft._EnemyColor == colorToDestroy && enemyLeft.isAlive == true)
                enemyLeft.setEnableParameters(false);
        }
    }

    //method to instance an enemy object
    private Enemy createEnemyInstance(float posX, float posY, Vector2 gridPos) {

        Enemy e = Enemy.Instantiate(EnemiesPrefab);
        e.transform.position = new Vector2(posX, posY);
        e.SetEnemyProperties(EnemyLife, EnemyColors, gridPos);
        _EnemiesParent = this.gameObject;
        e.transform.SetParent(_EnemiesParent.transform);
        return e;
    }

    private void FixedUpdate()
    {
        if (isGamePlayActive)
        {
            moveEnemies();
            shootOnlyBottomEnemies();

        }
        else
        {
            //showGameOverPanel(); *-*
        }
    }
    private int lastRowToShoot;//it's the last row with alive enemies.
    //this detect all enemies in bottom and activate their own shoot method
    private void shootOnlyBottomEnemies() {
        Debug.Log("lastRowToShoot: " + lastRowToShoot);
        for (int i = 0; i < enemyArray_.Length; i++)
        {

            if (lastRowToShoot >= 0)
            {
                if (enemyArray_[lastRowToShoot, i].GetComponent<SpriteRenderer>().enabled == true)
                {
                    enemyArray_[lastRowToShoot, i].needsToShoot = true;
                }
                else
                {
                    if (enemyArray_[lastRowToShoot - 1, i].GetComponent<SpriteRenderer>().enabled == true)
                    {
                        enemyArray_[lastRowToShoot - 1, i].needsToShoot = true;
                    }
                    else
                    {
                        if (enemyArray_[lastRowToShoot - 2, i].GetComponent<SpriteRenderer>().enabled == true)
                        {
                            enemyArray_[lastRowToShoot - 2, i].needsToShoot = true;
                        }
                        else
                        {
                            if (enemyArray_[lastRowToShoot - 3, i].GetComponent<SpriteRenderer>().enabled == true)
                            {
                                enemyArray_[lastRowToShoot - 3, i].needsToShoot = true;
                            }
                        }
                    }
                }
            }
        }
        
            //if(enemyArray_isAlive)

    }

    //this controls the movement of the enemies between the invisibles walls to turn the direction and move down one position per time.
    private void moveEnemies()
    {
        bool IsOnCollideWalls = false;
        for (int j = 0; j < enemyList.Count; j++)
        {
            if (enemyList[j].transform.position.x <= Walls[1].transform.position.x)//-6
            {
                newPosX = 0.03f;
                direction = false;
                IsOnCollideWalls = true;
            }
            if (enemyList[j].transform.position.x >= Walls[0].transform.position.x)//6
            {
                newPosX = -0.03f;
                direction = true;
                IsOnCollideWalls = true;
            }
        }
        for (int j = 0; j < enemyList.Count; j++)
        {
            if (IsOnCollideWalls)//-6
            { 
                newPosY = - 0.3f;
                enemyList[j].transform.position = new Vector3(enemyList[j].transform.position.x, enemyList[j].transform.position.y + newPosY, 0);
            }

           enemyList[j].transform.position += new Vector3(newPosX, 0, 0f);

            if (enemyList[j].transform.position.y <=-4)
            {
                onEnemyWins();
            }
        }
    }

    private void onEnemyWins() {
        Debug.Log("Enemy WINS!!!");
        isGamePlayActive = false;
    }
    private void showGameOverPanel()
    {
        if(GameOverPanel.gameObject.activeSelf == false)
        GameOverPanel.GetComponent<GameObject>().SetActive(true);
       
    }
    private void clearAllParameters() {

    }

}
