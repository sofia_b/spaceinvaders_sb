﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text PlayerLife;
    public Text Score;
    public Text HighScore;
    public Player PlayerController;

    // Start is called before the first frame update
    void Start()
    {
        main();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerLife.text = PlayerController.life.ToString();
    }

    public static void main() {
       //instancio el constructor de Player 
       EnemiesController.Instance.initialSetup();
    }



}
